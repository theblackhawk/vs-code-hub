from flask          import Flask, render_template, session, redirect, request, make_response
from flask_cors     import CORS

from secrets        import token_hex
from datetime       import datetime

from src.auth       import LoginChecker, UsersInfo
from src.virtualize import Container, ContainerManager 

import os

app            = Flask(__name__)
CORS(app)
app.secret_key = token_hex(32)

CM             = ContainerManager()
UI             = UsersInfo()

SSL_PORT       = 443

"""
# General Execution Flow 
 1 - User gets to the login page
 2 - Script checks the /etc/shadow for the password 
 3     - In case of success, the program creates container for the user 
 4     - In case of failure, the program return a false page 
"""
@app.route("/")
def index():
    if 'user' in session.keys():
        login = session['user']['login']
        url_callback  = CM.userContainer[login].getUrl()
        return redirect(url_callback)
    else :
        return render_template("index.html")



@app.route("/verify", methods=['GET','POST'])
def verify():
    if request.method == "POST":
        if 'login' in request.form.keys() and 'password' in request.form:
            LC = LoginChecker()
            login     = request.form['login']
            password  = request.form['password']
            if LC.verifyUser(login,password): 
                # Generating acces tokens and creation time
                access_token  =  token_hex(32)
                creation_time =  datetime.now()
                # Registring new session
                session['user'] = {"login"        : login, 
                                   "created"      : creation_time }
                container_pass = CM.newContainer(login, UI) 
                url_callback   = CM.userContainer[login].getUrl()
                
                print("[+] URL : "+url_callback)
                print("[+] Password for container access  : {}".format(container_pass)) 

                return render_template("pass_page.html", password = container_pass, container_link = url_callback)
                #return redirect(url_callback)
            else:
                # Replace with login redirection
                return render_template("index.html", false_login = True)
        else: 
            return 'Please Submit the credentials'

@app.route("/kill_all", methods=['GET'])
def killAll():
    try:
        CM.killOldContainers()
        return "OK"
    except:
        return "[-] A problem occured ..."


if __name__ == "__main__":
    
    app.run(host="127.0.0.1")
