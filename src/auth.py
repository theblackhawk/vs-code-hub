import crypt

class UsersInfo:
    def __init__(self, passwd_path = "/etc/passwd"):
        self.path   = passwd_path
        self.users  = {}
        self.loadUsers()

    def loadUsers(self):
        file = open(self.path, 'r')
        for line in file.readlines():
            user_name, _, uid, guid, _, home_folder = line.split(":")[:6]
            self.users[user_name] = {"uid" : uid,
                                     "guid": guid, 
                                     "home_folder" : home_folder}

    def getUserID(self, user_name):
        return self.users[user_name]['uid']
    def getUserGID(self, user_name):
        return self.users[user_name]['guid']
    def getUserHome(self, user_name):
        return self.users[user_name]['home_folder']

    
        
        

class LoginChecker:

    def __init__(self):
        self.loadCredentials()
        print("[+] Credentials loaded successfully")

    def loadCredentials(self):
        self.passDict     = {}
        passwordFile = open("/etc/shadow", "r")
        for line in passwordFile.readlines():
            user, hashed_pass = line.split(":")[0], line.split(":")[1]
            self.passDict[user] =  hashed_pass

    def lastDollar(self,string):
        index = 0
        for i in range(len(string)):
            if string[i] == "$":
                index = i

        return index

    def verifyUser(self, username, password):
        if username not in self.passDict.keys():
            return False
        else:
            cut_index = self.lastDollar(self.passDict[username])
            salt      = self.passDict[username][:cut_index]
         
            if self.passDict[username] == crypt.crypt(password, salt):
                return True
            else:
                return False


    
# Tests

if __name__ == '__main__':

    UI = UsersInfo()
    print(UI.getUserHome('user1'))
