## This module should include all the necessary classes for executiong docker actions like :


# 1 - Docker container start and stop 
# 2 - Docker container parametrization ( Affect certain parameters depending on user )
# 3 - Docker Manager service ( An object that will orchestrate all other containers)

# Tests : On true connect => start container => give IP of the service 
#       : On disconenct   => close container
#
# Classes
#  => DockerContainer
#  => DockerManager

from datetime import datetime
from secrets  import token_hex 
import docker 


PROTOCOL    = "https"

MIN_PORT  = 8443
MAX_PORT  = 9000

ACTUAL_PORT = MIN_PORT

LIFE_TIME = 3600  

HOST_NAME = "ml4.mldl.domain.loc"


class Container: 
    def __init__(self, container, port):
        self.c    = container
        self.id   = self.c.id
        self.port = port
        self.creationTime = datetime.now() 

    def getUrl(self):
        return(PROTOCOL+'://'+HOST_NAME+'/vscont/'+str(self.port)+'/')
        #return "http://192.168.67.15:4214"

    def kill(self):
        self.c.kill()


class ContainerManager:
    def __init__(self):
        self.client          = docker.from_env()
        self.userContainer   = {}

    def startReverseProxy(self, port):
        self.client.containers.run('nginx:latest', 
                                    name        = "reverse-proxy",
                                    ports       = {'80/tcp'  : 80,
                                                   '443/tcp' : port},
                                    volumes     = {'/home/adam.mabrouk/vs-code-hub/reverse_proxy/nginx.conf': 
                                                   {'bind': '/etc/nginx/nginx.conf', 'mode': 'rw'},
                                                   '/home/adam.mabrouk/vs-code-hub/reverse_proxy/certifications/': 
                                                   {'bind': '/root/certifications/', 'mode': 'rw'}},
                                    detach      = True)


    def newContainer(self, user_name, users_info):

        global ACTUAL_PORT
        home     = users_info.getUserHome(user_name)
        user_id  = users_info.getUserID(user_name)
        group_id = users_info.getUserGID(user_name)
        
        password = token_hex(16)
        print("User name is {}".format(user_name))
        cnt     =     self.client.containers.run('linuxserver/code-server', 
                                          name        = "vs-code_"+user_name,
                                          ports       = {'8443/tcp': 4214},
                                          volumes     = {home: {'bind': home, 'mode': 'rw'}},
                                          environment = ['PUID={}'.format(user_id),
                                                         'PGID={}'.format(group_id)],
                                          detach      = True)
        self.userContainer[user_name] = Container(cnt, ACTUAL_PORT)
        
        ACTUAL_PORT += 1

        return password

    
    def killContainer(self, user_name):
        if user_name in self.userContainer:
            self.userContainer[user_name].kill()
            del self.userContainer[user_name]
    
    def killOldContainers(self): 
        for key in self.userContainer.keys():
            if (datetime.now() - self.userContainer[key].creationTime).total_seconds() > LIFE_TIME:
                self.userContainer[key].kill()
                del self.userContainer[key]

    




if __name__ == '__main__':
    client = docker.from_env()

    # Test for running 3 containers
    #client.containers.run('linuxserver/code-server', ports = {'8443/tcp': 8443}, , volumes = {'/home/user1/': {'bind': '/home/user1', 'mode': 'rw'}}, detach=True)
    #client.containers.run('linuxserver/code-server', ports = {'8443/tcp': 8444}, detach=True)
    #client.containers.run('linuxserver/code-server', ports = {'8443/tcp': 8445}, detach=True)

